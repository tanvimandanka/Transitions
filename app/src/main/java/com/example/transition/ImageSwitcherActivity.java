package com.example.transition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;

import com.example.tanvi.imageswitcher.R;

public class ImageSwitcherActivity extends AppCompatActivity {
    private ImageSwitcher mImageSwitcher;
    private Button mButtton;

    // Array of Image IDs to Show In ImageSwitcher
    int imageIds[] = {R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher};
    int count = imageIds.length;
    // to keep current Index of ImageID array
    int currentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_switcher);

        // get The references of Button and ImageSwitcher
        mButtton = (Button) findViewById(R.id.buttonNext);
        mImageSwitcher = (ImageSwitcher) findViewById(R.id.simpleImageSwitcher);
        // Set the ViewFactory of the ImageSwitcher that will create ImageView object when asked
        mImageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                // Create a new ImageView and set it's properties
                ImageView imageView = new ImageView(getApplicationContext());
                // set Scale type of ImageView to Fit Center
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                // set the Height And Width of ImageView To FIll PARENT
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
                return imageView;
            }
        });

        // Declare in and out animations and load them using AnimationUtils class
        Animation in = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);

        // set the animation type to ImageSwitcher
        mImageSwitcher.setInAnimation(in);
        mImageSwitcher.setOutAnimation(out);
        mImageSwitcher.setImageResource(imageIds[currentIndex]);

        // ClickListener for NEXT button
        // When clicked on Button ImageSwitcher will switch between Images
        // The current Image will go OUT and next Image  will come in with specified animation
        mButtton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                currentIndex++;
                //  Check If index reaches maximum then reset it
                if (currentIndex == count)
                    currentIndex = 0;
                mImageSwitcher.setImageResource(imageIds[currentIndex]); // set the image in ImageSwitcher
            }
        });
    }

}
