package com.example.transition;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ViewSwitcher;

import com.example.tanvi.imageswitcher.R;

public class ViewSwitcherActivity extends AppCompatActivity {

    private ViewSwitcher mViewSwitcher;
    Button mButtonNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_switcher);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // get The references of Button and ViewSwitcher
        mButtonNext = findViewById(R.id.buttonNext);
        mViewSwitcher =  findViewById(R.id.simpleViewSwitcher); // get the reference of ViewSwitcher

        // Declare in and out animations and load them using AnimationUtils class
        Animation in = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);

        // set the animation type to ViewSwitcher
        mViewSwitcher.setInAnimation(in);
        mViewSwitcher.setOutAnimation(out);


        // ClickListener for NEXT button
        // When clicked on Button ViewSwitcher will switch between views
        // The current view will go out and next view will come in with specified animation
        mButtonNext.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                mViewSwitcher.showNext();
            }
        });
    }
}



